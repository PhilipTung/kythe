// Documentation can be placed next to documented elements.
enum class C {
//- @"///< an enumerator" documents EnumeratorCr
//- @Cr defines EnumeratorCr
  Cr ///< an enumerator
};
