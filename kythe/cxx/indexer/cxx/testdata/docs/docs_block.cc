// Block-style documentation comments are indexed.

/** doc */
class C { };

//- DocBlock documents ClassC
//- DocBlock.loc/start 52
//- DocBlock.loc/end 62
//- ClassC.node/kind record
