// BCPL-flavored block comments are indexed.

/// doc
class C { };

//- BcplBlock documents ClassC
//- ClassC.node/kind record
//- BcplBlock.loc/start 46
//- BcplBlock.loc/end 53
